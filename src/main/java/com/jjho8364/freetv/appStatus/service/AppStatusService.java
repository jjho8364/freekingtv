package com.jjho8364.freetv.appStatus.service;

import java.util.HashMap;

import com.jjho8364.freetv.appStatus.dto.AppStatusDTO;

public interface AppStatusService {
	
	AppStatusDTO selectAppStatus(HashMap<String, String> map) throws Exception;
	
}
