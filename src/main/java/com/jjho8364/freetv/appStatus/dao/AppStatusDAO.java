package com.jjho8364.freetv.appStatus.dao;

import java.util.HashMap;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.jjho8364.freetv.appStatus.dto.AppStatusDTO;

@Mapper
@Repository
public interface AppStatusDAO {
	
	AppStatusDTO selectAppStatus(HashMap<String, String> map) throws Exception;
	
}
