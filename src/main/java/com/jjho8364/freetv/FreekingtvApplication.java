package com.jjho8364.freetv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreekingtvApplication {

	public static void main(String[] args) {
		SpringApplication.run(FreekingtvApplication.class, args);
		
		
		System.out.println("Application has been started!!");
		
	}

}
